import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 *
 * @author Acacio Correia and Pedro Inacio
 */
public class FinalSchnorrEllipticCurve {
    
    static BigInteger oBIcP;
    static BigInteger oBIcQ;
    static BigInteger oBIca;
    static BigInteger oBIcb;
    static ECPoint oECPG;
    static int it;
    
    public static void main(String[] args) {
        // Length for the random variables
        int iRandomValuesLength = 160;
        
        int portNumber = 6666;
        
        // INITIALIZATION STEP
        // Here we define the values that both the client and the server have access to and agreed on
        // These values include a prime and a Q that divides (P-1)
        // An elliptic curve and its parameters
        // And a value t, in which 2^t < Q, that represents the security on which the protocol will result
        // Parameters from Curve P-521 http://csrc.nist.gov/groups/ST/toolkit/documents/dss/NISTReCur.pdf        
        oBIcP  = new BigInteger("686479766013060971498190079908139321726943530014330540939" +
                                "446345918554318339765605212255964066145455497729631139148" +
                                "0858037121987999716643812574028291115057151");
        
        oBIcQ  = new BigInteger("595705260554171862429497014052353994193508776338253635682" +
                                "116982804239751298116578581083870264708333599082679542772" +
                                "5898030721345295869362317857");
        
        it = 80;
        
        oBIca = new BigInteger("-3");
        
        oBIcb = new BigInteger("051953eb9618e1c9a1f929a21a0b68540eea2da725b99b315f3b8b4899" +
                               "18ef109e156193951ec7e937b1652c0bd3bb1bf073573df883d2c34f1e" +
                               "f451fd46b503f00", 16);
        
        oECPG = new ECPoint(
                new BigInteger("c6858e06b70404e9cd9e3ecb662395b4429c648139053fb521f828af60" +
                               "6b4d3dbaa14b5e77efe75928fe1dc127a2ffa8de3348b3c1856a429bf9" +
                               "7e7e31c2e5bd66", 16),
                new BigInteger("11839296a789a3bc0045c8a5fb42c7d1bd998f54449579b446817afbd1" +
                               "7273e662c97ee72995ef42640c550b9013fad0761353c7086a272c2408" +
                               "8be94769fd16650", 16));
        
        // Check the number of arguments
        if(args.length == 2){
            
            if(args[0].equals("--supplicant")){
                if(args[1].equals("--generate-keys"))
                    generateKeys(iRandomValuesLength);
                else
                    suplicantMode(args[1], iRandomValuesLength, portNumber);
            }else
                System.out.println("ERROR: Invalid arguments!");
        }else if(args.length == 3){
            if(args[0].equals("--authenticator")){
                authenticatorMode(args[1], args[2], iRandomValuesLength, portNumber);
            }else
                System.out.println("ERROR: Invalid arguments!");
        }else
            System.out.println("ERROR: Invalid arguments!");
    }
    
    // Funtion responsible for the mode of key generation
    public static void generateKeys(int iRandomValuesLength){
        //ALICE
        
        // Generate a random number a between 0 and Q
        // Alice's Private Key
        BigInteger oBIa = getSecureRandom(oBIcQ, iRandomValuesLength);
        
        // Alice's Public Key
        // v = -a.G(modP)
        // A point in the curve
        ECPoint oECPv = ellipticCurveMultiplication(oECPG, oBIa);
        // The '-' in the calculation of v represents the negation of the value of the y of the point resulting from (a.G)
        oECPv = new ECPoint(oECPv.getAffineX(), oECPv.getAffineY().negate().add(oBIcP));
        
        System.out.println("--- BEGIN PRIVATE KEY ---");
        System.out.println(oBIa);
        
        System.out.println("\n--- BEGIN PUBLIC KEY ---");
        System.out.println("pk.x = " + oECPv.getAffineX());
        System.out.println("pk.y = " + oECPv.getAffineY());
    }
    
    public static void suplicantMode(String sk, int iRandomValuesLength, int portNumber){
        //ALICE
        
        // Receive as the second argument in decimal
        // Alice's Private Key
        BigInteger oBIa;
        
        try{
            oBIa = new BigInteger(sk);
        }catch(NumberFormatException e){
            System.out.println("ERROR: Invalid arguments!");
            return;
        }
        
        // MESSAGE EXCHANGE STEP
        
        // Generate a random number r between 0 and Q
        BigInteger oBIr = getSecureRandom(oBIcQ, iRandomValuesLength);
        
        // x = r.G(modP)
        // A point in the curve
        ECPoint oECPx = ellipticCurveMultiplication(oECPG, oBIr);
        
        String hostName = "localhost";
        
        try {
            Socket communicationSocket = new Socket(hostName, portNumber);
                
            // Output stream for writing from the socket
            ObjectOutputStream oOOS = new ObjectOutputStream(communicationSocket.getOutputStream());
            
            // Send these values throught the socket            
            oOOS.writeObject(oECPx);
            oOOS.flush();
            
            // Input stream for reading from the socket
            ObjectInputStream oOIS = new ObjectInputStream(communicationSocket.getInputStream());
            
            BigInteger oBIe = (BigInteger)oOIS.readObject();
            
            //ALICE

            // Alice receive's e
            // Checks if e is between 0 and 2^t        
            if(oBIe.compareTo(BigInteger.valueOf(0)) == -1 || oBIe.compareTo(BigInteger.valueOf(2).pow(it)) > 0){
                System.out.println("ERROR: Something wrong!");
                return;
            }
        
            // y = ae + r mod Q
            BigInteger oBIy = oBIa.multiply(oBIe).add(oBIr).mod(oBIcQ);
            
            oOOS.writeObject(oBIy);
            oOOS.flush();
            
            String finalMessage = (String)oOIS.readObject();
            
            System.out.println(finalMessage);
            
            // Closes the stream
            oOOS.close();
            
            // Closes the stream
            oOIS.close();
            
            // Closes the socket
            communicationSocket.close();
            
        } catch (ClassNotFoundException | IOException e) {
            System.err.println("ERROR: Something wrong!");
        }
    }
    
    public static void authenticatorMode(String pkx, String pky, int iRandomValuesLength, int portNumber){         
        try {
            ECPoint oECPv;
        
            try{
                oECPv = new ECPoint(new BigInteger(pkx), new BigInteger(pky));
            }catch(NumberFormatException e){
                System.out.println("ERROR: Invalid arguments!");
                return;
            }
            
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            
            // MESSAGE EXCHANGE STEP

            //BOB
            
            // Input stream for reading from the socket
            ObjectInputStream oOIS = new ObjectInputStream(clientSocket.getInputStream());
            
            ECPoint oECPx = (ECPoint)oOIS.readObject();

            // Generate a random number e with t bits between 0 and 2^t
            BigInteger oBIe = getSecureRandom(BigInteger.valueOf(2).pow(it), it);
            
            // Output stream for writing from the socket
            ObjectOutputStream oOOS = new ObjectOutputStream(clientSocket.getOutputStream());
            
            oOOS.writeObject(oBIe);
            oOOS.flush();
            
            BigInteger oBIy = (BigInteger)oOIS.readObject();
            
            ECPoint oECPz = ellipticCurveAddition(ellipticCurveMultiplication(oECPG, oBIy), ellipticCurveMultiplication(oECPv, oBIe));

            String finalMessage;
            
            if(oECPz.equals(oECPx)){
                finalMessage = "Authentication successful!";
                System.out.println(finalMessage);
            }else{
                finalMessage = "Authentication failed!";
                System.out.println(finalMessage);
            }
            
            oOOS.writeObject(finalMessage);
            oOOS.flush();
            
            // Closes the stream
            oOIS.close();
            
            // Closes socket
            oOOS.close();
            
            clientSocket.close();
            serverSocket.close();
            
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("ERROR: Something wrong!");
        }
    }
    
    public static BigInteger getSecureRandom(BigInteger limit, int bits){
        int bytes = bits/8;
        BigInteger oBIa = null;
        
        try{
            // Source: BobEncrypt.java
            SecureRandom oSRprng = SecureRandom.getInstance("SHA1PRNG");

            // Generate a number with size bytes
            oBIa = new BigInteger(oSRprng.generateSeed(bytes));

            // This ensures that the number is positive
            oBIa = oBIa.abs();

            // We also have to ensure its < limit (or <= limit-1)
            while(oBIa.compareTo(limit) != -1){
                oBIa = new BigInteger(oSRprng.generateSeed(bytes));
                oBIa = oBIa.abs();
            }
        }catch(NoSuchAlgorithmException e){
            System.out.println(e);
        }
        
        return oBIa;
    }
    
    // Verified the values
    public static BigInteger ellipticCurveS(ECPoint pointA, ECPoint pointB){
        /* s = (y2 - y1) * (x2 - x1)^-1 mod p,     A != B
             = (3 * x1^2 + a) * (2 * y1)^-1 mod p, A  = B, where a represents the parameter a in the chosen elliptic curve
        */
        
        if(pointA.equals(pointB)){
            // (3 * x1^2 + a)
            BigInteger a = BigInteger.valueOf(3).multiply(pointA.getAffineX().pow(2)).add(oBIca);
            
            // (2 * y1)^-1, ^-1 means modInverse
            BigInteger b = BigInteger.valueOf(2).multiply(pointA.getAffineY()).modInverse(oBIcP);
            
            return a.multiply(b).mod(oBIcP);
        }else{
            // (y2 - y1)
            BigInteger a = pointB.getAffineY().subtract(pointA.getAffineY());
            
            // (x2 - x1)^-1 -> (x2 - x1 + P)^-1 to ensure we get the positive solution
            BigInteger b = pointB.getAffineX().subtract(pointA.getAffineX()).add(oBIcP).modInverse(oBIcP);
            
            return a.multiply(b).mod(oBIcP);
        }
    }
    
    // Verified the values
    public static ECPoint ellipticCurveAddition(ECPoint pointA, ECPoint pointB){
        BigInteger oBICx, oBICy;
        
        // x3 = s^2 - x1 - x2 mod p
        oBICx = ellipticCurveS(pointA, pointB).pow(2).subtract(pointA.getAffineX()).subtract(pointB.getAffineX()).mod(oBIcP);
        
        // y3 = s(x1 - x3) - y1 mod p
        oBICy = ellipticCurveS(pointA, pointB).multiply(pointA.getAffineX().subtract(oBICx)).subtract(pointA.getAffineY()).mod(oBIcP);
        
        return new ECPoint(oBICx, oBICy);
    }
    
    // Verified and works well -> TODO: Can make it faster by using the bD.length
    public static int numberBits(BigInteger number){
        int i = 0;
        
        // While the number is smaller than 2^i
        while(BigInteger.valueOf(2).pow(i).subtract(BigInteger.valueOf(1)).compareTo(number) == -1){
            i++;
        }
        
        return i;
    }
    
    // Creates a boolean array in which each boolean represents a bit 1 = true, removing unnecessary 0's to the left
    public static boolean[] byteArrayToBooleanArray(byte[] bD, int numberBits){
        boolean[] b = new boolean[numberBits];
        int c = 0;
        
        // http://stackoverflow.com/questions/9354860/how-to-get-the-value-of-a-bit-at-a-certain-position-from-a-byte
        // Get each bit of a byte
        for(int i = 0; i < bD.length; i++)
            for(int j = 7; j > -1; j--){
                if(i == 0 && j >= numberBits % 8)
                    continue;
                
                int bit = (bD[i] >> j) & 1;
                
                // True if bit is 1
                b[c] = bit == 1;
                
                c++;
            }
        
        return b;
    }
    
    // Verified the results
    public static ECPoint ellipticCurveMultiplication(ECPoint pointA, BigInteger number){
        ECPoint T = pointA;
        
        int iBit;
        
        byte[] bD = number.toByteArray();
        
        // These number of bits is needed because the function only iterates in these bits not in all 8 bites of each byte
        int bits = numberBits(number);
        
        boolean[] b = byteArrayToBooleanArray(bD, bits);

        // Run throught the bit array from the left to the right except the first position
        for(int i = 1; i < bits; i++){
            boolean x = b[i];
            
            if(x)
                iBit = 1;
            else
                iBit = 0;

            // T = T + T mod p
            T = ellipticCurveAddition(T, T);

            if(iBit == 1)
                T = ellipticCurveAddition(T, pointA);
        }
        
        return T;
    }
}