

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.ECPoint;

/**
 *
 * @author Acacio Correia and Pedro Inacio
 */
public class SchnorrEllipticCurve {
    
    static BigInteger oBIcP;
    static BigInteger oBIcQ;
    static BigInteger oBIca;
    static BigInteger oBIcb;
    static ECPoint oECPG;
    static int it;
            
    public static void main(String[] args) {
        // Length for the parameteres
        int iParametersLength = 1024;
        // Length for the random variables
        int iRandomValuesLength = 160;
        
        // Some tests with a set of fixed parameters to test out the elliptic curve operations such as addition and multiplication
        //testEllipticCurveOperations();
        
        // INITIALIZATION STEP
        // Here we define the values that both the client and the server have access to and agreed on
        // These values include a prime and a Q that divides (P-1)
        // An elliptic curve and its parameters
        // And a value t, in which 2^t < Q, that represents the security on which the protocol will result
        // P and Q from https://tools.ietf.org/html/draft-hao-schnorr-00
        oBIcP  = new BigInteger("E0A67598CD1B763BC98C8ABB333E5DDA0CD3AA0E5E1FB5BA8A7B4EAB" +
                                "C10BA338FAE06DD4B90FDA70D7CF0CB0C638BE3341BEC0AF8A7330A3" +
                                "307DED2299A0EE606DF035177A239C34A912C202AA5F83B9C4A7CF02" +
                                "35B5316BFC6EFB9A248411258B30B839AF172440F32563056CB67A86" +
                                "1158DDD90E6A894C72A5BBEF9E286C6B", 16);
        
        oBIcQ  = new BigInteger("E950511EAB424B9A19A2AEB4E159B7844C589C4F", 16);
        
        it = 80;
        
//        System.out.println("P: " + oBIcP);
//        System.out.println("Q: " + oBIcQ);
        
        // Generate the values A and B randomly
        generateAB(iParametersLength);
        
//        System.out.println("A: " + oBIca);
//        System.out.println("B: " + oBIcb);
        
        // Generate the initial point of the curve
        oECPG = findECPoint(iParametersLength);
        
        //ALICE
        
        // Generate a random number a between 0 and Q
        // Alice's Private Key
        BigInteger oBIa = getSecureRandom(oBIcQ, iRandomValuesLength);
        
        // Alice's Public Key
        // v = -a.G(modP)
        // A point in the curve
        ECPoint oECPv = ellipticCurveMultiplication(oECPG, oBIa);
        // The '-' in the calculation of v represents the negation of the value of the y of the point resulting from (a.G)
        oECPv = new ECPoint(oECPv.getAffineX(), oECPv.getAffineY().negate().add(oBIcP));
        
        // MESSAGE EXCHANGE STEP
        
        // Generate a random number r between 0 and Q
        BigInteger oBIr = getSecureRandom(oBIcQ, iRandomValuesLength);
        
        // x = r.G(modP)
        // A point in the curve
        ECPoint oECPx = ellipticCurveMultiplication(oECPG, oBIr);
        
        //BOB
        
        // Bob receive's v in an authenticated way <- No need to guarantee this
        // Bob receive's x
        
        // Generate a random number e with t bits between 0 and 2^t
        BigInteger oBIe = getSecureRandom(BigInteger.valueOf(2).pow(it), it);
        
        //ALICE
        
        // Alice receive's e
        // Checks if e is between 0 and 2^t        
        if(oBIe.compareTo(BigInteger.valueOf(0)) == -1 || oBIe.compareTo(BigInteger.valueOf(2).pow(it)) > 0){
            System.out.println("ERROR: Something wrong!");
            return;
        }
        
        // y = ae + r mod Q
        BigInteger oBIy = oBIa.multiply(oBIe).add(oBIr);//.mod(oBIcQ);
        
        //BOB
        
        // Bob receive's y
        
        // z = y.G + e.v(modP)
        ECPoint oECPz = ellipticCurveAddition(ellipticCurveMultiplication(oECPG, oBIy), ellipticCurveMultiplication(oECPv, oBIe));
        
        System.out.println("Z: " + oECPz.getAffineX() + " " + oECPz.getAffineY());
        System.out.println("X: " + oECPx.getAffineX() + " " + oECPx.getAffineY());
        
        if(oECPz.equals(oECPx))
            System.out.println("Authentication successful!");
    }
    
    public static void testEllipticCurveOperations(){
        oBIcP  = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF", 16);
        oBIca  = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFC", 16);
        oBIcb  = new BigInteger("64210519E59C80E70FA7E9AB72243049FEB8DEECC146B9B1", 16);
        oECPG = new ECPoint(new BigInteger("BED5AF16EA3F6A4F62938C4631EB5AF7BDBCDBC3", 16),
                            new BigInteger("1667CB477A1A8EC338F94741669C976316DA6321", 16));
        
        // Tests for the correction of the funcions ellipticCurveMultiplication, ellipticCurveAddition and ellipticCurveS
        ECPoint p1 = new ECPoint(new BigInteger("188DA80EB03090F67CBF20EB43A18800F4FF0AFD82FF1012", 16), new BigInteger("07192B95FFC8DA78631011ED6B24CDD573F977A11E794811", 16));
        System.out.println("P1: " + p1.getAffineX().toString(16) + " " + p1.getAffineY().toString(16));
        
        ECPoint p2 = ellipticCurveAddition(p1, p1);
        System.out.println("P2: " + p2.getAffineX().toString(16) + " " + p2.getAffineY().toString(16));
        System.out.println("P2: " + ellipticCurveMultiplication(p1, BigInteger.valueOf(2)).getAffineX().toString(16) + " " + ellipticCurveMultiplication(p1, BigInteger.valueOf(2)).getAffineY().toString(16));
        
        ECPoint p3 = ellipticCurveAddition(p2, p1);
        System.out.println("P3: " + p3.getAffineX().toString(16) + " " + p3.getAffineY().toString(16));
        System.out.println("P3: " + ellipticCurveMultiplication(p1, BigInteger.valueOf(3)).getAffineX().toString(16) + " " + ellipticCurveMultiplication(p1, BigInteger.valueOf(3)).getAffineY().toString(16));
        
        ECPoint p4 = ellipticCurveAddition(p3, p1);
        System.out.println("P4: " + p4.getAffineX().toString(16) + " " + p4.getAffineY().toString(16));
        System.out.println("P4: " + ellipticCurveMultiplication(p1, BigInteger.valueOf(4)).getAffineX().toString(16) + " " + ellipticCurveMultiplication(p1, BigInteger.valueOf(4)).getAffineY().toString(16));
        
        ECPoint p5 = ellipticCurveAddition(p4, p1);
        System.out.println("P5: " + p5.getAffineX().toString(16) + " " + p5.getAffineY().toString(16));
        System.out.println("P5: " + ellipticCurveMultiplication(p1, BigInteger.valueOf(5)).getAffineX().toString(16) + " " + ellipticCurveMultiplication(p1, BigInteger.valueOf(5)).getAffineY().toString(16));
        
        System.out.println("P20: " + ellipticCurveMultiplication(p1, BigInteger.valueOf(20)).getAffineX().toString(16) + " " + ellipticCurveMultiplication(p1, BigInteger.valueOf(20)).getAffineY().toString(16));
        
        System.out.println("P112233445566778899: " + ellipticCurveMultiplication(p1, new BigInteger("112233445566778899")).getAffineX().toString(16) + " " + ellipticCurveMultiplication(p1, new BigInteger("112233445566778899")).getAffineY().toString(16));
        
        System.out.println("P6277101735386680763835789423176059013767194773182842284080: " + ellipticCurveMultiplication(p1, new BigInteger("6277101735386680763835789423176059013767194773182842284080")).getAffineX().toString(16) + " " + ellipticCurveMultiplication(p1, new BigInteger("6277101735386680763835789423176059013767194773182842284080")).getAffineY().toString(16));
    }
    
    public static ECPoint findECPoint(int length){
        // The variable for each attempt of x value for our G, initial point
        BigInteger oBIx = BigInteger.ONE;
        
        BigInteger oBIa;
        BigInteger oBIy;
        
        while(true){
            // Contains the result of the left side of the equation
            // Ax^3 + x^2 + B = y^2
            oBIa = oBIca.multiply(oBIx.pow(3)).add(oBIx.pow(2)).add(oBIcb).mod(oBIcP);
            
            // If P = 3 mod 4 -> http://course1.winona.edu/eerrthum/13Spring/SquareRoots.pdf
            if(oBIcP.mod(BigInteger.valueOf(4)).equals(BigInteger.valueOf(3))){
                
                // If gcd(a, P) = 1 then we know there's a solution
                if(oBIa.gcd(oBIcP).equals(BigInteger.ONE)){

                    oBIy = oBIa.modPow(oBIcP.subtract(BigInteger.valueOf(3)).divide(BigInteger.valueOf(4)).add(BigInteger.ONE), oBIcP);

                    if(oBIy.modPow(BigInteger.valueOf(2), oBIcP).equals(oBIa)){
                        break;
                    }
                }
            // The implemented algorithm only works if it is true
            }else{
                System.out.println("ERROR: Somethin wrong!");
                return null;
            }
            
            oBIx = oBIx.add(BigInteger.valueOf(1));
        }
        
        // Recalculate another point in the curve in order to increase the security of G
        ECPoint point = new ECPoint(oBIx, oBIy);
        point = ellipticCurveMultiplication(point, getSecureRandom(oBIcP, length));
        
        return point;
    }
    
    public static void generateAB(int length){
        oBIca = getSecureRandom(oBIcP, length);
        oBIcb = getSecureRandom(oBIcP, length);
        
        // Find a and b where 4a^3 + 27b^2 = 0 mod p
        while(oBIca.pow(3).multiply(BigInteger.valueOf(4)).add(oBIcb.pow(2).multiply(BigInteger.valueOf(27))).mod(oBIcP).equals(BigInteger.ZERO)){
            oBIca = getSecureRandom(oBIcP, length);
            oBIcb = getSecureRandom(oBIcP, length);
        
            System.out.println("A: " + oBIca);
            System.out.println("B: " + oBIcb);
            System.out.println("P: " + oBIcP);
            System.out.println("M: " + oBIca.pow(3).multiply(BigInteger.valueOf(4)).add(oBIcb.pow(2).multiply(BigInteger.valueOf(27))).mod(oBIcP));
        }
    }
    
    public static BigInteger getSecureRandom(BigInteger limit, int bits){
        int bytes = bits/8;
        BigInteger oBIa = null;
        
        try{
            // Source: BobEncrypt.java
            SecureRandom oSRprng = SecureRandom.getInstance("SHA1PRNG");

            // Generate a number with size bytes
            oBIa = new BigInteger(oSRprng.generateSeed(bytes));

            // This ensures that the number is positive
            oBIa = oBIa.abs();

            // We also have to ensure its < limit (or <= limit-1)
            while(oBIa.compareTo(limit) != -1){
                oBIa = new BigInteger(oSRprng.generateSeed(bytes));
                oBIa = oBIa.abs();
            }
        }catch(NoSuchAlgorithmException e){
            System.out.println(e);
        }
        
        return oBIa;
    }
    
    // Verified the values
    public static BigInteger ellipticCurveS(ECPoint pointA, ECPoint pointB){
        /* s = (y2 - y1) * (x2 - x1)^-1 mod p,     A != B
             = (3 * x1^2 + a) * (2 * y1)^-1 mod p, A  = B, where a represents the parameter a in the chosen elliptic curve
        */
        
        if(pointA.equals(pointB)){
            // (3 * x1^2 + a)
            BigInteger a = BigInteger.valueOf(3).multiply(pointA.getAffineX().pow(2)).add(oBIca);
            
            // (2 * y1)^-1, ^-1 means modInverse
            BigInteger b = BigInteger.valueOf(2).multiply(pointA.getAffineY()).modInverse(oBIcP);
            
            return a.multiply(b).mod(oBIcP);
        }else{
            // (y2 - y1)
            BigInteger a = pointB.getAffineY().subtract(pointA.getAffineY());
            
            // (x2 - x1)^-1 -> (x2 - x1 + P)^-1 to ensure we get the positive solution
            BigInteger b = pointB.getAffineX().subtract(pointA.getAffineX()).add(oBIcP).modInverse(oBIcP);
            
            return a.multiply(b).mod(oBIcP);
        }
    }
    
    // Verified the values
    public static ECPoint ellipticCurveAddition(ECPoint pointA, ECPoint pointB){
        BigInteger oBICx, oBICy;
        
        // x3 = s^2 - x1 - x2 mod p
        oBICx = ellipticCurveS(pointA, pointB).pow(2).subtract(pointA.getAffineX()).subtract(pointB.getAffineX()).mod(oBIcP);
        
        // y3 = s(x1 - x3) - y1 mod p
        oBICy = ellipticCurveS(pointA, pointB).multiply(pointA.getAffineX().subtract(oBICx)).subtract(pointA.getAffineY()).mod(oBIcP);
        
        return new ECPoint(oBICx, oBICy);
    }
    
    // Verified and works well -> TODO: Can make it faster by using the bD.length
    public static int numberBits(BigInteger number){
        int i = 0;
        
        // While the number is smaller than 2^i
        while(BigInteger.valueOf(2).pow(i).subtract(BigInteger.valueOf(1)).compareTo(number) == -1){
            i++;
        }
        
        return i;
    }
    
    // Creates a boolean array in which each boolean represents a bit 1 = true, removing unnecessary 0's to the left
    public static boolean[] byteArrayToBooleanArray(byte[] bD, int numberBits){
        boolean[] b = new boolean[numberBits];
        int c = 0;
        
        // http://stackoverflow.com/questions/9354860/how-to-get-the-value-of-a-bit-at-a-certain-position-from-a-byte
        // Get each bit of a byte
        for(int i = 0; i < bD.length; i++)
            for(int j = 7; j > -1; j--){
                if(i == 0 && j >= numberBits % 8)
                    continue;
                
                int bit = (bD[i] >> j) & 1;
                
                // True if bit is 1
                b[c] = bit == 1;
                
                c++;
            }
        
        return b;
    }
    
    // Verified the results
    public static ECPoint ellipticCurveMultiplication(ECPoint pointA, BigInteger number){
        ECPoint T = pointA;
        
        int iBit;
        
        byte[] bD = number.toByteArray();
        
        // These number of bits is needed because the function only iterates in these bits not in all 8 bites of each byte
        int bits = numberBits(number);
        
        boolean[] b = byteArrayToBooleanArray(bD, bits);

        // Run throught the bit array from the left to the right except the first position
        for(int i = 1; i < bits; i++){
            boolean x = b[i];
            
            if(x)
                iBit = 1;
            else
                iBit = 0;

            // T = T + T mod p
            T = ellipticCurveAddition(T, T);

            if(iBit == 1)
                T = ellipticCurveAddition(T, pointA);
        }
        
        return T;
    }
    
    // http://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java
    // Converts an hex string to a byte array
    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    
    // http://www.mkyong.com/java/how-to-convert-hex-to-ascii-in-java/
    private static String convertStringToHex(String str){
 
	  char[] chars = str.toCharArray();
 
	  StringBuilder hex = new StringBuilder();
	  for(int i = 0; i < chars.length; i++){
	    hex.append(Integer.toHexString((int)chars[i]));
	  }
 
	  return hex.toString();
    }
    
    private static String convertHexToString(String hex){
 
	  StringBuilder sb = new StringBuilder();
	  StringBuilder temp = new StringBuilder();
 
	  //49204c6f7665204a617661 split into two characters 49, 20, 4c...
	  for( int i=0; i<hex.length()-1; i+=2 ){
 
	      //grab the hex in pairs
	      String output = hex.substring(i, (i + 2));
	      //convert hex to decimal
	      int decimal = Integer.parseInt(output, 16);
	      //convert the decimal to character
	      sb.append((char)decimal);
 
	      temp.append(decimal);
	  }
	  //System.out.println("Decimal : " + temp.toString());
 
	  return sb.toString();
    }
    
    // Outputs the closest integer less than or equal to x
    private static int floor(float x){
        return (int)Math.floor(x);
    }
    
    private static BigInteger binaryExpansionStoBI(String s){
        return new BigInteger(s.getBytes());
    }
    
    private static String binaryExpansionBItoS(BigInteger z){            
        return new String(z.toByteArray());
    }
    
    // Considering s an ASCII String and length the number of bits for the prime to generate
    private static String update_seed(String s, int length, int seedLength){
//        byte[] b = s.getBytes();
//        
//        // Check if the size of the string s is 160 bits as required
//        if(b.length*8 != seedLength){
//            System.out.println("ERROR: Seed size (" + b.length*8 + ") incompatible must be 160 bits long!");
//            return null;
//        }
        
        BigInteger z = binaryExpansionStoBI(s);
        
        // Add 1 to z
        z = z.add(BigInteger.valueOf(1));
        
        // Mod 2^160
        z = z.mod(BigInteger.valueOf(2).pow(seedLength));
        
        String t = binaryExpansionBItoS(z);
        
        if(!binaryExpansionStoBI(t).equals(z))
            System.out.println("ERROR: BINARY EXPANSION WENT WRONG");
        
        return t;
    }
    
    private static BigInteger find_integer(String s, int length, int seedLength){
        int v = floor((length - 1)/(float)seedLength);
        int w = length - seedLength*v;
        
        try{
            MessageDigest oMD = MessageDigest.getInstance("SHA-1");
            
            //System.out.println("Seed: " + new BigInteger(s.getBytes()));
            
            // Sha1(s)
            byte h[] = oMD.digest(s.getBytes());
            
            // w/8 is the amount of bytes we need
            byte h_0[] = new byte[w/8];
            
            // 20 because thats the output from SHA1 in bytes
            byte h_[][] = new byte[v][20];
            
            if(w % 8 != 0)
                System.out.println("ERROR: Number of bits needed ain't divisible by 8!");
            
            // Get the w rightmost bits of h
            // From left to right
            for(int i = h.length - (w/8); i < h.length; i++){
                h_0[i - (h.length - (w/8))] = h[i];
            }
            // From right to left
//            for(int i = 0; i < (w/8); i++){
//                h_0[i] = h[h.length - i - 1];
//            }
            
            //System.out.println("Sha1(seed): " + new BigInteger(h));
            
            //System.out.println("H_0: " + new BigInteger(h_0));
            
            // Convert s to integer
            // HEX OR ASCII?
            BigInteger z = binaryExpansionStoBI(s);
            
            BigInteger z_i;
            String s_i;
            
            // Calculate all h_[i]
            for(int i = 1; i <= v; i++){
                // z_i = (z+i) mod 2^160                
                z_i = z.add(BigInteger.valueOf(i)).mod(BigInteger.valueOf(2).pow(160));
                s_i = binaryExpansionBItoS(z_i);
                
                h_[i-1] = oMD.digest(s_i.getBytes());
            }
            
            h = new byte[(w/8) + v*20];
            
            // Concatenate all h_[i] in h
            System.arraycopy(h_0, 0, h, 0, h_0.length);
            
            for(int i = 0; i < v; i++)
                for(int j = 0; j < 20; j++)
                    h[i*20 + j + w/8] = h_[i][j];
            
            // Convert h to integer and output it
            return new BigInteger(h);
            
        }catch(NoSuchAlgorithmException e){
            System.out.println(e);
        }
        
        return null;
    }
    
    private static BigInteger generatePrime(int length){
        BigInteger p = null;
        
        String seed = "3243F6A8885A308D313198A2E03707344A409382";
        
        while(true){        
            // NOT WORKING PROPERLY
            BigInteger c = find_integer(seed, length, 160);

            //System.out.println(c);

            if(c.compareTo(BigInteger.valueOf(0)) == -1 || c.compareTo(BigInteger.valueOf(2).pow(length)) > -1)
                System.out.println("ERROR: x out of range!");

            if(c.compareTo(BigInteger.valueOf(0)) > -1){
                p = c.nextProbablePrime();

                if(p.compareTo(c) == -1 || p.mod(BigInteger.valueOf(4)).equals(BigInteger.valueOf(3)))
                    System.out.println("ERROR: Prime");

                // If 2^(L-1) <= p <= 2^L - 1 then output p
                if(p.compareTo(BigInteger.valueOf(2).pow(length - 1)) > -1 && p.compareTo(BigInteger.valueOf(2).pow(length).subtract(BigInteger.valueOf(1))) < 1)
                    break;
            }
            
            seed = update_seed(seed, length, 160);
        }
        
        return p;  
    }
}
