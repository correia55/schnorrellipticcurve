import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 *
 * @author Acacio Correia and Pedro Inacio
 */
public class SchnorrEllipticCurve_Server {
    
    static BigInteger oBIcP;
    static BigInteger oBIcQ;
    static BigInteger oBIca;
    static BigInteger oBIcb;
    static ECPoint oECPG;
    static int it;
    
    public static void main(String[] args) {
        // INITIALIZATION STEP
        // Here we define the values that both the client and the server have access to and agreed on
        // These values include a prime and a Q that divides (P-1)
        // An elliptic curve and its parameters
        // And a value t, in which 2^t < Q, that represents the security on which the protocol will result
        // P and Q from https://tools.ietf.org/html/draft-hao-schnorr-00
        oBIcP  = new BigInteger("E0A67598CD1B763BC98C8ABB333E5DDA0CD3AA0E5E1FB5BA8A7B4EAB" +
                                "C10BA338FAE06DD4B90FDA70D7CF0CB0C638BE3341BEC0AF8A7330A3" +
                                "307DED2299A0EE606DF035177A239C34A912C202AA5F83B9C4A7CF02" +
                                "35B5316BFC6EFB9A248411258B30B839AF172440F32563056CB67A86" +
                                "1158DDD90E6A894C72A5BBEF9E286C6B", 16);
        
        oBIcQ  = new BigInteger("E950511EAB424B9A19A2AEB4E159B7844C589C4F", 16);
        
        it = 80;
        
        oBIca = new BigInteger("1f588ce50f8e5661de52c5f001fbef6119c9da3ae041679844d758dd" +
                               "d7bebcd030bc7e0fb72dc453d74e49b34c37142ad2f85584f449d3c9" +
                               "b3c2b5847cb135ad85bbe0c8d63a93986447321ea6f8c11124a7bc1d" +
                               "70bfe2ebe4c69c5282a68de9e38ad558bc7201561329e91bb2fd7429" +
                               "330efa0db472a15bea8544729ad8e85c", 16);
        
        oBIcb = new BigInteger("49e20051b0f06efa258845d7902691c0f4b13fe5e0107d40bf89b8f6" +
                               "a8334a8b2ffd3a469bb4a934cf8141e5185f302103f25b46ed175acd" +
                               "a261fc8cfc0ea35ad48e595c495978abd0e9b1cd11db5bd1a5348b0b" +
                               "bd64423fa1739a26fffa84c1f58b04eccceac383b0f68887830b73cf" +
                               "f1f37f6659cce1560afebed5c2408a66", 16);
        
        oECPG = new ECPoint(
                new BigInteger("5c90a5d6e9485cc992d16b4c7f5a92c38e24662438c58a4d968a4a57" +
                               "b57d15cf3e2a028271a9acd819f55acb6715720692d367912edb3cd4" +
                               "4749beabe94ce7b92f2aaedd4ec41638584fa1a79e4e7c3fadae79ab" +
                               "824ee57e1b59d0c742662b11107063e32286f70c934f198fd54030cd" +
                               "6e5b7eb7daa3bf6981e64c18559b692a", 16),
                new BigInteger("21f18b032e59e69ec4b9c657b4ebffef9efe66a429af290ba400667e" +
                               "1c2632ec336ade4f0ffe1e94125139246b2e803b094bd5a7d73fd8ae" +
                               "cfcc7c77b0bf50c22dfd5bba4d6d0b1643441aec70b3f4d69247c1e3" +
                               "29b45cc5dc3f2d8987b44edaa217769e93d78addc2ebd6d3b7d79e90" +
                               "8d3701732c4a1611a62cfa1c804a8915", 16));
         
        int portNumber = 6666;
         
        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            
            // MESSAGE EXCHANGE STEP

            //BOB

            // Bob receive's v in an authenticated way <- No need to guarantee this
            
            // Input stream for reading from the socket
            ObjectInputStream oOIS = new ObjectInputStream(clientSocket.getInputStream());
            
            ECPoint oECPv = (ECPoint)oOIS.readObject();
            
            ECPoint oECPx = (ECPoint)oOIS.readObject();

            // Generate a random number e with t bits between 0 and 2^t
            BigInteger oBIe = getSecureRandom(BigInteger.valueOf(2).pow(it), it);
            
            // Output stream for writing from the socket
            ObjectOutputStream oOOS = new ObjectOutputStream(clientSocket.getOutputStream());
            
            oOOS.writeObject(oBIe);
            oOOS.flush();
            
            BigInteger oBIy = (BigInteger)oOIS.readObject();
            
            ECPoint oECPz = ellipticCurveAddition(ellipticCurveMultiplication(oECPG, oBIy), ellipticCurveMultiplication(oECPv, oBIe));

            String finalMessage;
            
            if(oECPz.equals(oECPx)){
                finalMessage = "Authentication successful!";
                System.out.println(finalMessage);
            }else
                finalMessage = "Authentication failed!";
            
            oOOS.writeObject(finalMessage);
            oOOS.flush();
            
            // Closes the stream
            oOIS.close();
            
            // Closes socket
            oOOS.close();
            
            clientSocket.close();
            serverSocket.close();
            
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("ERROR: Something wrong!");
        }
    }
    
    public static BigInteger getSecureRandom(BigInteger limit, int bits){
        int bytes = bits/8;
        BigInteger oBIa = null;
        
        try{
            // Source: BobEncrypt.java
            SecureRandom oSRprng = SecureRandom.getInstance("SHA1PRNG");

            // Generate a number with size bytes
            oBIa = new BigInteger(oSRprng.generateSeed(bytes));

            // This ensures that the number is positive
            oBIa = oBIa.abs();

            // We also have to ensure its < limit (or <= limit-1)
            while(oBIa.compareTo(limit) != -1){
                oBIa = new BigInteger(oSRprng.generateSeed(bytes));
                oBIa = oBIa.abs();
            }
        }catch(NoSuchAlgorithmException e){
            System.out.println(e);
        }
        
        return oBIa;
    }
    
    // Verified the values
    public static BigInteger ellipticCurveS(ECPoint pointA, ECPoint pointB){
        /* s = (y2 - y1) * (x2 - x1)^-1 mod p,     A != B
             = (3 * x1^2 + a) * (2 * y1)^-1 mod p, A  = B, where a represents the parameter a in the chosen elliptic curve
        */
        
        if(pointA.equals(pointB)){
            // (3 * x1^2 + a)
            BigInteger a = BigInteger.valueOf(3).multiply(pointA.getAffineX().pow(2)).add(oBIca);
            
            // (2 * y1)^-1, ^-1 means modInverse
            BigInteger b = BigInteger.valueOf(2).multiply(pointA.getAffineY()).modInverse(oBIcP);
            
            return a.multiply(b).mod(oBIcP);
        }else{
            // (y2 - y1)
            BigInteger a = pointB.getAffineY().subtract(pointA.getAffineY());
            
            // (x2 - x1)^-1 -> (x2 - x1 + P)^-1 to ensure we get the positive solution
            BigInteger b = pointB.getAffineX().subtract(pointA.getAffineX()).add(oBIcP).modInverse(oBIcP);
            
            return a.multiply(b).mod(oBIcP);
        }
    }
    
    // Verified the values
    public static ECPoint ellipticCurveAddition(ECPoint pointA, ECPoint pointB){
        BigInteger oBICx, oBICy;
        
        // x3 = s^2 - x1 - x2 mod p
        oBICx = ellipticCurveS(pointA, pointB).pow(2).subtract(pointA.getAffineX()).subtract(pointB.getAffineX()).mod(oBIcP);
        
        // y3 = s(x1 - x3) - y1 mod p
        oBICy = ellipticCurveS(pointA, pointB).multiply(pointA.getAffineX().subtract(oBICx)).subtract(pointA.getAffineY()).mod(oBIcP);
        
        return new ECPoint(oBICx, oBICy);
    }
    
    // Verified and works well -> TODO: Can make it faster by using the bD.length
    public static int numberBits(BigInteger number){
        int i = 0;
        
        // While the number is smaller than 2^i
        while(BigInteger.valueOf(2).pow(i).subtract(BigInteger.valueOf(1)).compareTo(number) == -1){
            i++;
        }
        
        return i;
    }
    
    // Creates a boolean array in which each boolean represents a bit 1 = true, removing unnecessary 0's to the left
    public static boolean[] byteArrayToBooleanArray(byte[] bD, int numberBits){
        boolean[] b = new boolean[numberBits];
        int c = 0;
        
        // http://stackoverflow.com/questions/9354860/how-to-get-the-value-of-a-bit-at-a-certain-position-from-a-byte
        // Get each bit of a byte
        for(int i = 0; i < bD.length; i++)
            for(int j = 7; j > -1; j--){
                if(i == 0 && j >= numberBits % 8)
                    continue;
                
                int bit = (bD[i] >> j) & 1;
                
                // True if bit is 1
                b[c] = bit == 1;
                
                c++;
            }
        
        return b;
    }
    
    // Verified the results
    public static ECPoint ellipticCurveMultiplication(ECPoint pointA, BigInteger number){
        ECPoint T = pointA;
        
        int iBit;
        
        byte[] bD = number.toByteArray();
        
        // These number of bits is needed because the function only iterates in these bits not in all 8 bites of each byte
        int bits = numberBits(number);
        
        boolean[] b = byteArrayToBooleanArray(bD, bits);

        // Run throught the bit array from the left to the right except the first position
        for(int i = 1; i < bits; i++){
            boolean x = b[i];
            
            if(x)
                iBit = 1;
            else
                iBit = 0;

            // T = T + T mod p
            T = ellipticCurveAddition(T, T);

            if(iBit == 1)
                T = ellipticCurveAddition(T, pointA);
        }
        
        return T;
    }
}
