# Schnorr Identification Protocol over Elliptic Curves

This repository contains a Java implementation of the Schnorr Identification Protocol over Elliptic Curves. It includes a program that may either act as a server (the authenticator), which waits for a client to try to authenticate, or as a client (the supplicant) which tries to connect to the server and perform authentication, provided the right parameters. The program also allows for the generation of a pair of keys for the execution of the system.

The available source files are the following:

* ECPoint.java - contained at java.security.spec.ECPoint with the implementation of the Serializable interface (which enables transmission via sockets);

* SchnorrEllipticCurve_Client.java - with the implementation of the supplicant part of the protocol;

* SchnorrEllipticCurve_Server.java - with the implementation of the authenticator part of the protocol;

* SchnorrEllipticCurve.java - with the implementation of the complete protocol as a single process, not making the distinction between client and server;

* FinalSchnorrEllipticCurve.java - with the main function, structuring the functionalities via command line options (see User Manual).


### Security Considerations ###

This implementation uses fixed parameters and replaces one of the cyclic groups over the integers module a large prime by a group over elliptic curves. The curve used is one of the recommended curves by NIST in http://csrc.nist.gov/groups/ST/toolkit/documents/dss/NISTReCur.pdf, namely the one termed as P-521. It defines a cyclic group with an order r with 521 bits. r-1 is divided by a large prime Q, with 470 bits, which we used for the cyclic group over the integers modulo Q. The parameters fulfill the security requirements defined for the protocol, namely the ones mentioned in the Internet draft by F. Hao in https://tools.ietf.org/html/draft-hao-schnorr-00#section-3. A small number of tests were performed to the implementation and it worked as it should for all of them. Nonetheless, we make no statements regarding its security or correctness. We paid no attention to side channel attack prevention mechanisms. 


### User Manual ###

This is the manual for using the program in FinalSchnorrEllipticCurve.java. The program offers three main functionalities over two modes:

* Supplicant Mode (Client)

The generation of a pair of keys can be done via a command similar to the following one (the generated keys are printed in the standard output):
```
$ java FinalSchnorrEllipticCurve.java --supplicant --generate-keys
```

The supplicant part of the protocol can be started using the command:
```
$ java FinalSchnorrEllipticCurve.java --supplicant secret_key_value
```

* Authenticator Mode (Server)

The authenticator (server) should be started prior to the supplicant with a command similar to:
```
$ java FinalSchnorrEllipticCurve.java --authenticator x_value_point_public_key y_value_point_public_key
```

### Authors ###
Acácio F. P. P. Correia and Pedro R. M. Inácio

For questions, please contact acaciofilipe.correia@gmail.com.